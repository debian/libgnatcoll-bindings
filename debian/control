Source: libgnatcoll-bindings
Priority: optional
Section: libdevel
Maintainer: Nicolas Boulenguez <nicolas@debian.org>
Build-Depends:
 debhelper-compat (= 13),
 dh-ada-library (>= 9.1),
 dh-sequence-ada-library,
 gnat,
 gnat-14,
 gprbuild (>= 2025.0.0-2),
# Implicit libc6-dev provides: iconv, omp, syslog.
 libgmp-dev,
 libgnatcoll-dev,
 pkgconf,
 python3-dev,
 libpython3-dev,
 libreadline-dev,
 liblzma-dev,
 zlib1g-dev,
Homepage: https://github.com/AdaCore/gnatcoll-bindings
Standards-Version: 4.7.2
Rules-Requires-Root: no
Vcs-Browser: https://salsa.debian.org/debian/libgnatcoll-bindings
Vcs-Git: https://salsa.debian.org/debian/libgnatcoll-bindings.git

######################################################################

Package: libgnatcoll-python3-dev
Breaks: libgnatcoll-python3-1-dev, libgnatcoll-python3-2-dev
Replaces: libgnatcoll-python3-1-dev, libgnatcoll-python3-2-dev
Provides: ${ada:Provides}
Architecture: any
# The Python version we were built with may differ from the default.
Depends: ${misc:Depends}, ${ada:Depends}, libpython${python-version}-dev,
Description: Ada binding to the Python3 language (development)
 The GNAT Component Collection deals with: module tracing, efficient
 file IO, static string searching (Boyer-Moore), e-mails and
 mailboxes, Ravenscar tasking profiles, storage pools, JSON, logging,
 shell scripting. Components relying on external dependencies are
 distributed in separate packages.
 .
 This package contains the static library and Ada specifications
 adding Python as scripting language.

Package: libgnatcoll-python3-2
Section: libs
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Multi-Arch: same
Description: Ada binding to the Python3 language (runtime)
 The GNAT Component Collection deals with: module tracing, efficient
 file IO, static string searching (Boyer-Moore), e-mails and
 mailboxes, Ravenscar tasking profiles, storage pools, JSON, logging,
 shell scripting. Components relying on external dependencies are
 distributed in separate packages.
 .
 This package contains the runtime shared library for Python
 scripting.

######################################################################

Package: libgnatcoll-iconv-dev
Breaks: libgnatcoll-iconv17-dev, libgnatcoll-iconv18-dev,
 libgnatcoll-iconv19-dev, libgnatcoll-iconv20-dev, libgnatcoll-iconv21-dev
Replaces: libgnatcoll-iconv17-dev, libgnatcoll-iconv18-dev,
 libgnatcoll-iconv19-dev, libgnatcoll-iconv20-dev, libgnatcoll-iconv21-dev
Provides: ${ada:Provides}
Architecture: any
Depends: ${misc:Depends}, ${ada:Depends}
Description: Ada binding for Iconv character encoding conversions (development)
 The GNAT Component Collection deals with: module tracing, efficient
 file IO, static string searching (Boyer-Moore), e-mails and
 mailboxes, Ravenscar tasking profiles, storage pools, JSON, logging,
 shell scripting. Components relying on external dependencies are
 distributed in separate packages.
 .
 This package contains the static library and Ada specifications for
 the binding with the Iconv character encoding conversion library.

Package: libgnatcoll-iconv21
Section: libs
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Multi-Arch: same
Description: Ada binding for Iconv character encoding conversions (runtime)
 The GNAT Component Collection deals with: module tracing, efficient
 file IO, static string searching (Boyer-Moore), e-mails and
 mailboxes, Ravenscar tasking profiles, storage pools, JSON, logging,
 shell scripting. Components relying on external dependencies are
 distributed in separate packages.
 .
 This package contains the runtime shared library for the iconv
 binding.

######################################################################

Package: libgnatcoll-readline-dev
Breaks: libgnatcoll-readline17-dev, libgnatcoll-readline18-dev,
 libgnatcoll-readline19-dev, libgnatcoll-readline20-dev,
 libgnatcoll-readline21-dev
Replaces: libgnatcoll-readline17-dev, libgnatcoll-readline18-dev,
 libgnatcoll-readline19-dev, libgnatcoll-readline20-dev,
 libgnatcoll-readline21-dev
Provides: ${ada:Provides}
Architecture: any
Depends: ${misc:Depends}, ${ada:Depends}, libreadline-dev,
Description: Ada binding for ReadLine input history (development)
 The GNAT Component Collection deals with: module tracing, efficient
 file IO, static string searching (Boyer-Moore), e-mails and
 mailboxes, Ravenscar tasking profiles, storage pools, JSON, logging,
 shell scripting. Components relying on external dependencies are
 distributed in separate packages.
 .
 This package contains the static library and Ada specifications
 for the binding with GNU readline and history libraries.

Package: libgnatcoll-readline21
Section: libs
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Multi-Arch: same
Description: Ada binding for ReadLine input history (runtime)
 The GNAT Component Collection deals with: module tracing, efficient
 file IO, static string searching (Boyer-Moore), e-mails and
 mailboxes, Ravenscar tasking profiles, storage pools, JSON, logging,
 shell scripting. Components relying on external dependencies are
 distributed in separate packages.
 .
 This package contains the runtime shared library for the readline
 binding.

######################################################################

Package: libgnatcoll-gmp-dev
Breaks: libgnatcoll-gmp17-dev, libgnatcoll-gmp18-dev, libgnatcoll-gmp19-dev,
 libgnatcoll-gmp20-dev, libgnatcoll-gmp21-dev
Replaces: libgnatcoll-gmp17-dev, libgnatcoll-gmp18-dev, libgnatcoll-gmp19-dev,
 libgnatcoll-gmp20-dev, libgnatcoll-gmp21-dev
Provides: ${ada:Provides}
Architecture: any
Depends: ${misc:Depends}, ${ada:Depends}, libgmp-dev,
Description: Ada binding for GMP big numbers (development)
 The GNAT Component Collection deals with: module tracing, efficient
 file IO, static string searching (Boyer-Moore), e-mails and
 mailboxes, Ravenscar tasking profiles, storage pools, JSON, logging,
 shell scripting. Components relying on external dependencies are
 distributed in separate packages.
 .
 This package contains the static library and Ada specifications
 for a binding with the GNU Multiple Precision arithmetic C library.

Package: libgnatcoll-gmp21
Section: libs
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Multi-Arch: same
Description: Ada binding for GMP big numbers (runtime)
 The GNAT Component Collection deals with: module tracing, efficient
 file IO, static string searching (Boyer-Moore), e-mails and
 mailboxes, Ravenscar tasking profiles, storage pools, JSON, logging,
 shell scripting. Components relying on external dependencies are
 distributed in separate packages.
 .
 This package contains the runtime shared library for the GMP binding.

######################################################################

Package: libgnatcoll-syslog-dev
Breaks: libgnatcoll-syslog1-dev, libgnatcoll-syslog2-dev,
 libgnatcoll-syslog3-dev, libgnatcoll-syslog4-dev, libgnatcoll-syslog5-dev
Replaces: libgnatcoll-syslog1-dev, libgnatcoll-syslog2-dev,
 libgnatcoll-syslog3-dev, libgnatcoll-syslog4-dev, libgnatcoll-syslog5-dev
Provides: ${ada:Provides}
Architecture: any
Depends: ${misc:Depends}, ${ada:Depends}
Description: Ada binding for syslog journals (development)
 The GNAT Component Collection deals with: module tracing, efficient
 file IO, static string searching (Boyer-Moore), e-mails and
 mailboxes, Ravenscar tasking profiles, storage pools, JSON, logging,
 shell scripting. Components relying on external dependencies are
 distributed in separate packages.
 .
 This package contains the static library and Ada specifications
 for an interface to the syslog journal manager daemon.

Package: libgnatcoll-syslog4
Section: libs
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Multi-Arch: same
Description: Ada binding for syslog journals (runtime)
 The GNAT Component Collection deals with: module tracing, efficient
 file IO, static string searching (Boyer-Moore), e-mails and
 mailboxes, Ravenscar tasking profiles, storage pools, JSON, logging,
 shell scripting. Components relying on external dependencies are
 distributed in separate packages.
 .
 This package contains the runtime shared library for the syslog binding.

######################################################################

Package: libgnatcoll-lzma-dev
Breaks: libgnatcoll-lzma1-dev, libgnatcoll-lzma2-dev, libgnatcoll-lzma3-dev,
 libgnatcoll-lzma4-dev
Replaces: libgnatcoll-lzma1-dev, libgnatcoll-lzma2-dev, libgnatcoll-lzma3-dev,
 libgnatcoll-lzma4-dev
Provides: ${ada:Provides}
Architecture: any
Depends: ${misc:Depends}, ${ada:Depends}, liblzma-dev,
Description: Ada binding for LZMA compression (development)
 The GNAT Component Collection deals with: module tracing, efficient
 file IO, static string searching (Boyer-Moore), e-mails and
 mailboxes, Ravenscar tasking profiles, storage pools, JSON, logging,
 shell scripting. Components relying on external dependencies are
 distributed in separate packages.
 .
 This package contains the static library and Ada specifications
 for the binding with the lzma compression library.

Package: libgnatcoll-lzma3
Section: libs
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Multi-Arch: same
Description: Ada binding for LZMA compression (runtime)
 The GNAT Component Collection deals with: module tracing, efficient
 file IO, static string searching (Boyer-Moore), e-mails and
 mailboxes, Ravenscar tasking profiles, storage pools, JSON, logging,
 shell scripting. Components relying on external dependencies are
 distributed in separate packages.
 .
 This package contains the runtime shared library for the lzma binding.

######################################################################

Package: libgnatcoll-zlib-dev
Breaks: libgnatcoll-zlib1-dev, libgnatcoll-zlib2-dev, libgnatcoll-zlib3-dev,
 libgnatcoll-zlib4-dev
Replaces: libgnatcoll-zlib1-dev, libgnatcoll-zlib2-dev, libgnatcoll-zlib3-dev,
 libgnatcoll-zlib4-dev
Provides: ${ada:Provides}
Architecture: any
Depends: ${misc:Depends}, ${ada:Depends}, zlib1g-dev,
Description: Ada binding for Zlib compression (development)
 The GNAT Component Collection deals with: module tracing, efficient
 file IO, static string searching (Boyer-Moore), e-mails and
 mailboxes, Ravenscar tasking profiles, storage pools, JSON, logging,
 shell scripting. Components relying on external dependencies are
 distributed in separate packages.
 .
 This package contains the static library and Ada specifications
 for the binding with the zlib compression library.

Package: libgnatcoll-zlib3
Section: libs
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Multi-Arch: same
Description: Ada binding for Zlib compression (runtime)
 The GNAT Component Collection deals with: module tracing, efficient
 file IO, static string searching (Boyer-Moore), e-mails and
 mailboxes, Ravenscar tasking profiles, storage pools, JSON, logging,
 shell scripting. Components relying on external dependencies are
 distributed in separate packages.
 .
 This package contains the runtime shared library for the zlib binding.

######################################################################

Package: libgnatcoll-omp-dev
Breaks: libgnatcoll-omp1-dev, libgnatcoll-omp2-dev, libgnatcoll-omp3-dev
Replaces: libgnatcoll-omp1-dev, libgnatcoll-omp2-dev, libgnatcoll-omp3-dev
Provides: ${ada:Provides}
Architecture: any
Depends: ${misc:Depends}, ${ada:Depends}
Description: Ada binding for OpenMP parallelism (development)
 The GNAT Component Collection deals with: module tracing, efficient
 file IO, static string searching (Boyer-Moore), e-mails and
 mailboxes, Ravenscar tasking profiles, storage pools, JSON, logging,
 shell scripting. Components relying on external dependencies are
 distributed in separate packages.
 .
 This package contains the static library and Ada specifications
 for the binding with the OpenMP parallelism library.

Package: libgnatcoll-omp2
Section: libs
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Multi-Arch: same
Description: Ada binding for OpenMP parallelism (runtime)
 The GNAT Component Collection deals with: module tracing, efficient
 file IO, static string searching (Boyer-Moore), e-mails and
 mailboxes, Ravenscar tasking profiles, storage pools, JSON, logging,
 shell scripting. Components relying on external dependencies are
 distributed in separate packages.
 .
 This package contains the runtime shared library for the OpenMP binding.

######################################################################

Package: libgnatcoll-cpp-dev
Provides: ${ada:Provides}
Architecture: any
Depends: ${misc:Depends}, ${ada:Depends},
Description: Ada binding to C++ standard library functions
 The GNAT Component Collection deals with: module tracing, efficient
 file IO, static string searching (Boyer-Moore), e-mails and
 mailboxes, Ravenscar tasking profiles, storage pools, JSON, logging,
 shell scripting. Components relying on external dependencies are
 distributed in separate packages.
 .
 This package contains the static library and Ada specifications
 wrapping some functions of the C++ standard library.

Package: libgnatcoll-cpp1
Section: libs
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Multi-Arch: same
Description: Ada binding to C++ standard library functions (runtime)
 The GNAT Component Collection deals with: module tracing, efficient
 file IO, static string searching (Boyer-Moore), e-mails and
 mailboxes, Ravenscar tasking profiles, storage pools, JSON, logging,
 shell scripting. Components relying on external dependencies are
 distributed in separate packages.
 .
 This package contains the runtime shared library wrapping some
 functions of the standard C++ library.
